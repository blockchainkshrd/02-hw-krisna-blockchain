package config

import (
	"context"
	_ "github.com/go-kivik/couchdb/v3" // The CouchDB driver
	kivik "github.com/go-kivik/kivik/v3"
)

var DB *kivik.DB

func Handelcouchdb() {

	client, err := kivik.New("couch", "http://admin:root@35.184.96.31:5984/")
	print("client", client, err)

	if err != nil {
		panic(err)
	}

	//auto create database if not exit
	client.CreateDB(context.TODO(), "db_student")
	//tell application to used database name db_student
	db := client.DB(context.TODO(), "db_student")

	DB = db
}
